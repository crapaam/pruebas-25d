extends Spatial


var rot_speed = 5
var speed = .5
enum STATUS {IDLE, RUNNING, JUMPING, FALL}
var status = STATUS.IDLE
enum DIR {LEFT, UP, RIGHT, DOWN, IDLE}
var dir = DIR.IDLE


func _ready():
	
	randomize()


func _physics_process(delta):

	if Input.is_action_pressed("ui_right"):
		$Arm.rotation_degrees.y += rot_speed
	
	if Input.is_action_pressed("ui_left"):
		$Arm.rotation_degrees.y -= rot_speed
	
	move_runner(delta)


func move_runner(delta):
	
	if dir == DIR.UP:
		$Runner.translation += Vector3(1, 0, 0) * speed * delta
		$Runner.rotation_degrees = Vector3(0, 0, 0)
	
	if dir == DIR.DOWN:
		$Runner.translation += Vector3(-1, 0, 0) * speed * delta
		$Runner.rotation_degrees = Vector3(0, 90, 0)
		
	if dir == DIR.RIGHT:
		$Runner.translation += Vector3(0, 0, 1) * speed * delta
		$Runner.rotation_degrees = Vector3(0, 180, 0)
		
	if dir == DIR.LEFT:
		$Runner.translation += Vector3(0, 0, -1) * speed * delta
		$Runner.rotation_degrees = Vector3(0, 270, 0)
		

func _on_Timer_timeout():
	
	dir = randi() % 5
	print(dir)
	if dir < 4:
		$Runner/AnimationPlayer.play("Running-loop")
	else:
		$Runner/AnimationPlayer.play("Happy Idle-loop")
