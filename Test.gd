extends Spatial


var speed = 1
var clouds_speed = .2
var background_speed = .5
var speed_y = 0
var gravity = -.1
var jump_y = 1.6
var on_floor = true
var game_over = false
enum STATUS {IDLE, RUNNING, JUMPING, FALL}
var status = STATUS.IDLE
var pressed = false


func _ready():
	
	$Runner/AnimationPlayer.play("Running-loop")


func _physics_process(delta):
	
	if status == STATUS.RUNNING or status == STATUS.JUMPING:
	
		# mover edificios
		
		$Fondo.translation.x += -1 * clouds_speed * delta
		
		if $Fondo.translation.x < -10:
			$Fondo.translation.x = 0
		
		# mover piso
		
		$Pisos.translation.x += -1 * speed * delta
		
		if $Pisos.translation.x < -10:
			$Pisos.translation.x = 0
		
		# mover nubes
		
		$Clouds.translation.x += -1 * clouds_speed * delta
		
		if $Clouds.translation.x < -4:
			$Clouds.translation.x = 4
		
		# mover cono
		
		$Cone.translation.x += -1 * speed * delta
		
		if $Cone.translation.x < -2:
			$Cone.translation.x = 3
		
		# mover runner
		
		if Input.is_action_just_pressed("ui_accept"):
			
			pressed = true
		
		if pressed and on_floor:
			speed_y = jump_y
			on_floor = false
			print(speed_y)
			$Runner/AnimationPlayer.play("Jump-loop")
			pressed = false
		if $Runner.translation.y > 0:
			speed_y += gravity
		if $Runner.translation.y < 0:
			print("en suelo")
			on_floor = true
			$Runner.translation.y = 0
			speed_y = 0
			$Runner/AnimationPlayer.play("Running-loop")
		
		$Runner.translation.y += speed_y * delta
	
	elif status == STATUS.IDLE:
			
			$Runner/AnimationPlayer.play("Happy Idle-loop")
			
			if Input.is_action_just_pressed("ui_accept"):
				
				pressed = true
			
			if pressed:
				
				status = STATUS.RUNNING
				$Runner/AnimationPlayer.play("Running-loop")
			
			
	elif status == STATUS.FALL:
		
		pass
		


func _on_Area_area_entered(area):
	
	status = STATUS.FALL
	$Runner/AnimationPlayer.get_animation("Fall Over-loop").loop = false
	$Runner/AnimationPlayer.play("Fall Over-loop")
	$Timer.start()


func _on_Timer_timeout():
	
	get_tree().change_scene("res://Test.tscn")


func _on_Button_pressed():
	
	
	pass


func _on_Button_button_down():
	
	pressed = true


func _on_Button2_button_down():
	
	$WorldEnvironment/DirectionalLight.shadow_enabled = ! $WorldEnvironment/DirectionalLight.shadow_enabled
